const jwt =require('jsonwebtoken');
const configs = require('./../configs');
const UserModel = require('./../models/user.model');
module.exports = function(req,res,next){
    // if(req.query.token==='random'){
    //     next();
    // }else{
    //     next({
    //         msg:"you are not authenticate",
    //         status:401
    //     })
    // }
    let token;
    if(req.headers['authorization'])
     token = req.headers['authorization']
    if(req.headers['x-access-token'])
     token = req.headers['x-access-token']
    if(req.query.token)
     token = req.query.token
    token = (token || '').split(' ')[1];
    if(!token){
        return next({
            msg:'Authentication faild token tot provide',
            status:403
        })
    } 
    //token verification
    jwt.verify(token,configs.JWT_SECRET,function(err,decoded){
        if(err){
            return next(err)
        }
        console.log('Token verification successful',decoded)
        UserModel.findById(decoded._id, { password: 0 }, function (err, user) {
            if (err) {
              return next(err);
            }
            if (!user) {
              return next({
                msg: 'User removed from system',
                status: 404
              })
            }
            req.user = user;
            return next();
      
        })
    })
}