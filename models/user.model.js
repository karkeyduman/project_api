const mongoose = require('mongoose');
//  const uniqueValidator = require('mongoose-unique-validator');// yedi sidai unique validation not wirking then use this 
//new mongoose.Schema; yo garepani hunxa nava 
const Schema = mongoose.Schema;

const UserSchema = new Schema({
//   name:String
 name:{
    type:String
},
 username:{
    type:String,
    required:true,
    unique: true,
    trim:true,
    lowercase:true
 },
 password:{
    type:String,
    required:true
 },
 email:{
    type:String,
    unique:true,
    sparse:true // empty value lai skip gardinxa 
 },
 gender:{
    type:String,
    enum:['male','female','others']
 },
 dob:{
    type: Date
 },
 isMarried:{
    type:Boolean
 },
 nationality:{
    type:String,
    default:'nepali'
 },
 phoneNumber:{
    type:Number
   
 },
 address:{
    temporaryAddress:[String],//array as a string value j pani huna sakxa 
    permanentAddress:{
        type:String
    }

 },
 status:{
    type:String,
    enum:['active','inactive'],
    default:'active'
 },
 role:{
    type:Number,
    default:2 //1 for admin ,2 for normal user
 },
  image:{
   type:String
  },
  passworResetExpire: Date
},{timestamps:true})
//   UserSchema.plugin(uniqueValidator);
const UserModel = mongoose.model('user',UserSchema); // user vanne automation collection banaidinxa plural form ma ani userSchema le schema define garxa 
module.exports = UserModel;
