module.exports = function(userData,user){
if(userData.name)
 user.name = userData.name;
if(userData.username)
 user.username = userData.username;
if(userData.password)
 user.password = userData.password;
if(userData.email)
 user.email =userData.email;
if(userData.phoneNumber)
 user.phoneNumber =userData.phoneNumber;
if(userData.dob)
 user.dob =userData.dob;
if(userData.isMarried)
 user.isMarried = userData.isMarried;
if(!user.address)
 user.address={};
if(userData.tempAddr)
 user.address.temporaryAddress=userData.tempAddr.split(',');
if(userData.permanentAddr)
 user.address.permanentAddress =userData.permanentAddr;
if(userData.role)
 user.role= userData.role;
if(userData.status)
 user.status=userData.status;
if(userData.nationaliy)
 user.nationaliy =userData.nationaliy;
 if(userData.image)
 user.image =userData.image;
return user;
}