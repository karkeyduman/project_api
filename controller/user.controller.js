const router = require('express').Router();
const dbConfigs = require('./../configs/db.configs');
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../helpers/mapUserReq');
const uploader = require('./../middlewares/uploader')
const Authorize = require('./../middlewares/authorize')

router.route('/')
.get(function(req,res,next){
 UserModel
 .find({},{username:1})
 .sort({
   _id:-1 // gives a data in decending order 
 })
 .exec(function(err,users){
   if(err){
     return next(err)
   }
   res.json(users)
 })
   
})
.post(function(req,res,next){

})


router.route('/search')
.get(function(req,res,next){
res.send('from get request from search handler in user controller')
})
.post(function(req,res,next){

});

router.route('/:id')
.get(function(req,res,next){
//  res.send('from dynamin handler ')
UserModel
.find({
   _id:req.params.id
})
.exec(function(err,users){
   if(err){
      return next(err)
   }
   res.json(users)
})


})
.put(uploader.single('image'), function(req,res,next){
//update ko lagi 
UserModel
.findById(req.params.id,function(err,user){
   if(err){
      return next(err)
   }
   if(!user){
      return next({
         msg:"User Not Found for Update",
         status:404
      })
   }
   if(req.fileTypeErr){
      return next({
           msg:'Invalid file format',
           status:400
       })
   }
   
   //database ma pathauna 
   let Oldimage;
   if(req.file){
       req.body.image = req.file.filename
       Oldimage=user.image;
   }
const updateMapUser=MapUserReq(req.body, user)
updateMapUser.save(function(err,done){
    if(err){
        return next(err)
    }
    if(req.file){
      require('fs').unlink(process.cwd(),'uploads/images/'+Oldimage,function(err,done){
         if(!err){
            console.log('file removed')
         }
      })
    }
    res.json(done)
})
})
})
.delete(Authorize, function (req, res, next) {
   UserModel.findById(req.params.id, function (err, user) {
     if (err) {
       return next(err);
     }
     if (!user) {
       return next({
         msg: 'User Not Found',
         status: 404
       })
     }
     user.remove(function (err, done) {
       if (err) {
         return next(err)
       }
       res.json(done);
     })
   })
 });



module.exports = router;
// always put static handler at top never put below dynamic handlere