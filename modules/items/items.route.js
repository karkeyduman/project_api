const router = require('express').Router();
const ItemCtrl = require('./items.controller');
const uploader = require('./../../middlewares/uploader')
const authenticate=require('./../../middlewares/authenticate');

router.route('/')
.get(authenticate,ItemCtrl.getAllItem)
.post(authenticate,uploader.array('images'), ItemCtrl.insert);


router.route('/:id')
.get(authenticate,ItemCtrl.getById)
.put(authenticate,uploader.array('images'),ItemCtrl.update)
.delete(authenticate,ItemCtrl.remove)

router.route('/add_ratings/:item_id')
.post(authenticate,ItemCtrl.addRatings)
router.route('/search')
.get(ItemCtrl.search)
.post(ItemCtrl.search);
 
module.exports= router;