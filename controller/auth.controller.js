const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model')
const mapUserReqt = require('./../helpers/mapUserReq');
const Uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt= require('jsonwebtoken');
const configs = require('./../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'karkeyduman@gmail.com',
      pass: 'uayycnwqnkrrzaql'
    }
  })
  
  function prepareEmail(data) {
    return {
      from: 'hello IMS', // sender address
      to: "karkeyduman@gmail.com," + data.email, // list of receivers
      subject: "Forgot Password ✔", // Subject line
      text: "Forgot Password", // plain text body
      html: `
        <div>
        <p>Hi <strong> ${data.name} </strong>,</p>
        <p>We noticed that you are having trouble logging into our system.please use link below to reset your password</p>
        <p><a href="${data.link}">cliclk here to reset your password</a></p>
        <p>Warm Regards</p>
        <p> hello IMS Support Team</p>
        <p>2022</p>
        </div>
      `, // html body
    }
  }
  


function getToken(data){
   return jwt.sign({
        username: jwt.username,
        role: jwt.role,
        _id:data._id
    },configs.JWT_SECRET,{
        //option such as 
        // expiresIn:
    })
}
    
    
router.post('/login',function(req,res,next){
    if(!req.body.username){
        next({
            msg:'please provide username'
        })
    }
    UserModel
    .findOne({
        $or:[
            {
                username:req.body.username.trim().toLowerCase()
            },
            {
                email:req.body.username
            }
        ]
    })
    .then(function(user){
        if(!user){
          return next({
            msg:'invalid username',
            status:400
          })
        }
        //password verification
        let isMatched = passwordHash.verify(req.body.password,user.password);
        if(!isMatched){
            return next({
                msg:'invalid password',
                status:400
            })
        }
         // token generation
      let token = getToken(user)
      res.json({
        token: token,
        user: user
      })
    })
    .catch(err=>{
       next(err)
    })
    
})  

router.post('/register',Uploader.single('image'), function(req,res,next){
    console.log('req.body-->',req.body)
    console.log('single file--->',req.file)
    console.log('multiple file--->',req.files)
    if(req.filetypeErr){
        return next({
            msg:'Invalid file format',
            status:400
        })
    }
    //database ma pathauna 
    if(req.file){
        req.body.image = req.file.filename
    }
    
    // const requestData = req.body;
    const newUser = new UserModel({}) // object banayeko object banauda var obj ={name:'abc'}or obj.addr='bkt 'garepani hunxa 
    const newMapUser = mapUserReqt(req.body, newUser)
    newUser.password=passwordHash.generate(req.body.password)

    newMapUser.save(function(err,done){
    if(err){
        return next(err)
    }
    res.json(done)
})
    
})
router.post('/forgot_password',function(req,res,next){
 UserModel.findOne({
    email:req.body.email
 }, function(err,user){
    if(err){
        return next(err)
    }
    if(!user){
        return next({
            msg:'Email not Register Yet',
            status:404
        })
    }
    let passworResetExpireTime =Date.now()+1000*60*60*24;
    // email proceed now 
    let emailData={
        name:user.username,
        email:user.email,
        link:`${req.headers.origin}/reset_password/${user._id}`// last value is dynamic value for confirmation for rignt
    }
    var emailContent=prepareEmail(emailData);
    user.passworResetExpire=passworResetExpireTime
    user.save(function(err,done){
        if(err){
            return next(err)
        }
        sender.sendMail(emailContent,function(err,done){
            if(err){
                 return next(err)
                // console.log('errr',err)
            }
            else{
                res.json(done)
            }
        })
    })
 })
})
router.post('/reset_password/:id', function (req, res, next) {
  let id= req.params.id;
  UserModel.findOne({
    _id:id,
    $gte:Date.now()
  },function(err,user){
    if(err){
        return next(err)
    }
    if(!user){
        return next({
            msg:'Invalid or Password reset link expired',
            status:400
        })
    }
    // let now  = Date.now();
    // if(user.passwordResetExpiry <= now){
    //   // proceed
    // }

    //user fond update password
    user.password = passwordHash.generate(req.body.password);
    user.passworResetExpire=null;  //in 24 hours, it allow password reset only one time through link
    user.save(function(err,done){
        if(err){
            return next(err)
        }else{
            res.json(done)
        }
    })
  })
})
  
module.exports=router;
