// database is a simply bucket or container  to hold a data
//to manage database we use database system management to manipulate a data  these are 
//1. relational database management system 
//2. distributed database management system

//database modeling 
//LMS
//--> entity ---> attributes
// E.R diagram

// 1. relational dbms ---> table based design
//-->entiries for library management system (LMS)-->eg book,reviews,user,rating etc
//-->each table inside table is called tuples or rows
//-->we need schema based design 
//--> schema defines the properties and its datatypes(eg for user rntity property will be user_id,password,dob,username etc) 
//-->relation between table exist
//--> non scalable-->data jati pani rakhna pahiyo ra entity pahila define gareko vanda aru entity add garna paedaina 
//--> eg database --> mysql,sql lite,postgres,sql-server
//--> sql databae (structure query language)

//2. Distributed DBMS
//-->collection base design
//-->eg book ,notification,review --> collecton 
//-->each record inside a collection aslo called document so it is also called document baase 
//-->each document is valid JSON or object
//--> schema base design
//-->relation between collection doesnot exist
//-->highly scalable--> no schema base designd so we can easily store valid json
//-->eg database mongodb,couchdb, dynamobd,redis (in memory database ram vitra pani manage garna milxa data using redis)

// to download mongodb
//-->download ==> compass (uncheck)
//-->UI tool ==> compass ,robo 3t ,studio 3t,
// window case after download file c drive ko program files ma hunxa download gareko kura
//eg inside program file mongodb>version>bin>===>bin vitra executable file hunxa 
// copy the path to bin and we should  add this path in system variable section
//

// MONGODB ===> on of the most popular database ,it is also called no sql dbms
//client -server communication in mongodb
// mongobd server ==> 27017 server port will be listening 
//to connect with mongodb server mongo command from any location eg from cmd 
//interface establish once connection will be started 

//mongoshell  command 
// show dbs ==> it shows all the available database present in mongo server 
// use<db_name> ==> if(db exist it use the existing database ) else ceate a new database and select it
//db ==> it shows connected or selected database 
// show connections ==> it shows the available connections 

//CRUD operation 
//db.<connections_name>.insert({valid json})
//eg db.laptops.insert({name:'ideapad',generation:'i5',tags:["lenevo","ideapad","i5"],status:"available"})

//Read 
//db.<collections_name>.find({}) it shows all data
// eg db.<collections_name>.find({query_builder}) it gives data according to query builder(like search from specific character )
////db.<collections_name>.find({color:'red'})// it gives red color item 
//db.<collections_name>find({query_builder}).pretty()==>formats output dinxa 
//db.<collections_name>find({query_builder}).count()==>return result length

//SORT
//db.<collections_name>.find({}).sort({_id:-1}) ===> it gives the inserted valued in descending order last ma insert vayeko 1st ma aauxa and so on
//db.<collections_name>.find({}).limit(10)==> it shows inserted data in 10 rows format
//db.<collecions_name>.find({}).skip(14) ==> it skip the value from 1 to 14 and shows data after 14 
//db.<collections_name>.find({tags:{$in:['a','b','i5']}}) ==> it show a data if tags vitra ko 'a'or 'b' or 'i5' kunai eauta match vayo vane match vayeko data dekhauxa 
// db.<collections_name>.find({tags:{$all:['a','b','i5']}}) ==>$all garyo vane tags vitra ko sabai kura match vayo vane matra match vayeko result  dekhauxa 

// to find from array 
//$gt ==> greater than
//$gte ==> greater than or eqalto 
//$lt ==> less than 
//$lte ==> less than or equal to 
//eg db.laptops.find({$or:[{brand:"lenevo"},{brand:"acer"}]}) ==> it shows the both brand lenevo or acer
// db.<collections_name>.find({},{brand:1})==> it is also called projections if we used this only brand matra output dinxa
// db.<collections_name>.find({},{brand:0}) garyo vane brand matra didaina aru sabai dinxa 


// UPDATE operation
//db.<collectons_name>.update({boj1},{obj2},{obj3})
//eg db.laptops.update({_id:ObjectId('id ko value')},{$set:{status:'sold'}})
//1st object ==> query builder
//2nd object ===>key value pair
// key as $set and value as payload(object) to be updated
// eg db.laptops.update({},{$set{status:'sold'}}) status sold update hunxa 
// 3rd object ==> option ==> optional  
// eg db.laptops.update({},{$set:{status:'solt'}},{multi:true})==> garyo vane sabai value ko status update hunxa 
//eg db.laptops.update({},{$set:{status:'solt'}},{multi:true,upsert:true}) ==> upsert:true garyo vane status navayeko data aayo vane pani update gardinxa yedi purano version ko mongo xa vane nava upsert:true chahidaina


// DELETE Operation
// db.<collection_name>.remove({query builder})//yedi query builder rakhena vane sabai data delete hunxa 

// DROP operation
// db.<collections_name>.drop(); ==>collection drop hunxa 
//db.dropDatabase(); ===> database drop hunxa 

//mongoose ==> ODM (object document modeling)
//Advantages
//1. schema based solution
//2. datatype
//3. middlewares
//4. method
//5. indexing lot easier ==> kun lai unique banaune etc 

//######### DATABASE  BACKUP  AND RESTORE ########
// it has four command these are
// mongodump ,mongorestore
//mongoexport, mongoimport

// 2 ota data type bata restore garna sakina hhese are bson vaneko machine readable and json/csv are human readable
//bson   and json / csv

//bson 
//backup 
// command
//mongodump ===> it craeate a backup folder with bson data 
// hami jun folder ma basera comman gari ra xau tehi dump folder create garidinxa to store bson data
//// mongodump --db <selected database name> // backups selected database inside default dump folder
// mongodump --db <selected database name> --out < path to destination_folder> 
// path can be any dynamic name

// RESTORE
// mongorestore ==> it will look after dump folder to restore bson backup database
// mongorestore --drop ==> restore incoming document droping duplicated existing documents
// mongorestore <path_to-source folder> // restore from other then dump folder


// JSON 
// backup ==>
// command  mongoexport 
// mongoexport --db <db_name> --collection<collection_name> --out <path_to_destinatio_with.json extension>

// restore 
// mongoimport --db[new or existing db] -- collection <new or existing db> <path to  json source file>

// CSV
// command ==> mongoexport and mongoimport
// backup
// command mongoexport
// mongoexport --db <db_name> --collection<collection_name>  --query='{key:"value",key:"value"}' --type=csv --fields 'comma seperated propertyname to indentify column header' --out <path-to-destination-with .csv extension>
// mongoexport -d <db_name> -c <collection_name> --csv -q ='{"key":"value"}' -f 'comma seperated propertyname to indentify column header' -o <path-to-destination-with .csv extension>


// restore
// mongoimport --db<new or existing db> --colection <new or existing collection> --type=csv <path_to_csv_source_file> --headerline



//######### DATABASE  BACKUP  AND RESTORE ########
