const router = require('express').Router();

const authRouter = require('./../controller/auth.controller');
const userRouter = require('./../controller/user.controller');
const itemRouter =require('./../modules/items/items.route');

// importning middlewares 
const authenticate=require('./../middlewares/authenticate');
const isAminRouter = require('./../middlewares/isAdmin');

router.use('/auth',authRouter)
router.use('/user',authenticate, userRouter)
router.use('/item',itemRouter)
router.use('/review',isAminRouter, userRouter) 
router.use('/notification',userRouter)
router.use('/message',userRouter)

module.exports=router;