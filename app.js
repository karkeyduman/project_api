const express = require ('express');
const morgan = require('morgan');
const path = require('path');
const cors= require('cors');

//import routing level middleware
const apiRouter = require('./routes/api.routes')

const app = express();
const PORT = 4000;

require('./db_init'); 
app.use(cors());// accept all request
app.use(morgan("dev"));

 app.use('/file',express.static(path.join(process.cwd(),'uploads')))
 // to parse a incomming data
 // eg urlencoded parser    
 // this middleware will parse x-ww-formurl-encoded incomming data 
 app.use(express.urlencoded({
    extended:true
 }))
 app.use(express.json())

app.use('/api', apiRouter)


app.use(function(req,res,next){
    next({
        msg:'Not Found',
        status:404
    
    })
})
app.use(function(err,req,res,next){
    res.status(err.status || 400)
    res.json({ 
        status:err.status || 400,
        msg: err.msg || err
    })
})
app.listen(PORT,function(err,done){
    if(err){
        console.log('error in port listening'+err)
    }
    else{
        console.log('success in port listening--->'+PORT)
    }
})