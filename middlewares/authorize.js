module.exports = function(req,res,next){
    if(req.user.role ===1){
        next()
    }else{
        next({
            msg:'you don`t have access to delete',
            status:403
        })
    }
}