const multer = require('multer')
const path = require('path')
//file filter before uploads 
//filefilter never block req res cycle
function imageFilter(req,file,cb){
   var mime_type=file.mimetype.split('/')[0];
   if(mime_type==='image'){
    cb(null,true)
   }else
    {   
        req.filetypeErr = true;
        cb(null,false)
   }
}
function sizeFilter(req,file,cb){
   
    if(file.size>=3000){
        cb(null,true)
    }else{
        req.sizeTypeErr = true;
        cb(null,false)
    }
}
//to full control use disc storage in image or file uploading
const myStorage = multer.diskStorage({
    filename:function(req,file,cb){
        cb(null, Date.now()+'-'+file.originalname)
    },
    destination: function(req,file,cb){
        cb(null,path.join(process.cwd(),'uploads/images'))
    }
})

const upload = multer({
    storage:myStorage,
    fileFilter:imageFilter,
    imageSizeFilter:sizeFilter
})

module.exports =upload;